package models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "request")
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "requestId")
    private Integer id;

    private LocalDate requestDate;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User userRequest;

    @ManyToOne
    @JoinColumn(name = "books_library_id")
    private Book book_request;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
    }

    public User getUserRequest() {
        return userRequest;
    }

    public void setUserRequest(User userRequest) {
        this.userRequest = userRequest;
    }

    public Book getBook_request() {
        return book_request;
    }

    public void setBook_request(Book book_request) {
        this.book_request = book_request;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", bookRequest=" + book_request +
                ", requestDate=" + requestDate +
                '}';
    }
}
