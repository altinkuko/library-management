package Repository;

import models.Book;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.List;

public class BookRepository implements Repository<Book, Integer> {

    private final Session session;

    public BookRepository(Session session) {
        this.session = session;
    }

    @Override
    public Book findById(Integer id) {
        try {
            return session.find(Book.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void create(Book model) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(model);
            System.out.println("Book has been register");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    @Override
    public void update(Integer id) {
        Transaction transaction = null;
        Book book;
        try {
            transaction = session.beginTransaction();
            book = session.get(Book.class, id);
            session.update(book);
            System.out.println("Book has been updated");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Integer id) {
        Book book;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            book = session.get(Book.class, id);
            session.delete(book);
            System.out.println("Book with id " + id + " has been deleted");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }
@Override
    public List<Book> read() {
        Transaction transaction = null;
        List<Book> bookList = null;
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            bookList = session.createQuery("from Book").list();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return bookList;
    }
}
