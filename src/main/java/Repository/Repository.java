package Repository;

import java.util.List;

public interface Repository <T, ID> {

    T findById(ID id);

    void create(T model);

    void update(ID id);

    void delete(ID id );

    List<T> read();
}
