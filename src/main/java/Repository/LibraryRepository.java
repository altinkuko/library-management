package Repository;

import models.Library;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.List;

public class LibraryRepository implements Repository<Library,Integer>{

    private final Session session;

    public LibraryRepository(Session session) {
        this.session = session;
    }
    @Override
    public Library findById(Integer id) {
        try {
            return session.find(Library.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void create(Library model) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(model);
            System.out.println("Library has been register");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    @Override
    public void update(Integer id) {
        Transaction transaction = null;
        Library library;
        try {
            transaction = session.beginTransaction();
            library = session.get(Library.class, id);
            session.update(library);
            System.out.println("Library has been updated");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Integer id) {
        Library library;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            library = session.get(Library.class, id);
            session.delete(library);
            System.out.println("Library with id "+id+" has been deleted");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }
@Override
    public List<Library> read() {
        Transaction transaction = null;
        List<Library> libraryList = null;
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            libraryList = session.createQuery("from Library").list();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return libraryList;
    }


}
