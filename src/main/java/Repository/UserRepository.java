package Repository;

import models.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;
import java.util.List;
public class UserRepository implements Repository<User, Integer> {
    private final Session session;
    public UserRepository(Session session){
        this.session = session;
    }
    @Override
    public User findById(Integer id) {
        try{
            return session.find(User.class, id);
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public void create(User model) {
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.save(model);
            System.out.println("User has been registered");
            transaction.commit();
        } catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    @Override
    public void update(Integer id) {
        Transaction transaction = null;
        User user;
        try{
            transaction = session.beginTransaction();
            user = session.get(User.class,id);
            session.update(user);
            System.out.println("User has been updated");
            transaction.commit();
        } catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    @Override
    public void delete(Integer id) {
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.delete(id);
            transaction.commit();
        } catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    @Override
    public List<User> read() {
        Transaction transaction = null;
        List<User> users = null;
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            users = session.createQuery("from User").list();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return users;
    }
}
