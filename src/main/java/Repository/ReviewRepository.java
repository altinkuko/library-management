package Repository;

import models.Review;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.List;

public class ReviewRepository implements Repository<Review, Integer> {

    private final Session session;

    public ReviewRepository(Session session) {
        this.session = session;
    }

    @Override
    public Review findById(Integer id) {
        try {
            return session.find(Review.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void create(Review model) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(model);
            System.out.println("Review has been register");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    @Override
    public void update(Integer id) {
        Transaction transaction = null;
        Review review;
        try {
            transaction = session.beginTransaction();
            review = session.get(Review.class, id);
            session.update(review);
            System.out.println("Review has been updated");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Integer id) {
        Review review;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            review = session.get(Review.class, id);
            session.delete(review);
            System.out.println("Review with id " + id + " has been deleted");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }
@Override
    public List<Review> read() {
        Transaction transaction = null;
        List<Review> reviewList = null;
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            reviewList = session.createQuery("from Review").list();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return reviewList;
    }

}
