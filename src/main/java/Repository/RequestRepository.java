package Repository;

import models.Request;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.List;

public class RequestRepository implements  Repository<Request,Integer> {
    private final Session session;

    public RequestRepository(Session session) {
        this.session = session;
    }
    @Override
    public Request findById(Integer id) {
        try {
            return session.find(Request.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;


    }

    @Override
    public void create(Request model) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(model);
            System.out.println("Request has been register");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }


    }


    @Override
    public void update(Integer id) {
        Transaction transaction = null;
        Request request;
        try {
            transaction = session.beginTransaction();
            request = session.get(Request.class,id);
            session.update(request);
            System.out.println("Request with id "+id+" has been updated");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }



    @Override
    public void delete(Integer id) {
        Request request;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            request = session.get(Request.class,id);
            session.delete(request);
            System.out.println("Request with id "+id+" has been deleted");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }
    @Override
    public List<Request> read() {
        Transaction transaction = null;
        List<Request> requestList = null;
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            requestList = session.createQuery("from Request").list();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return requestList;
    }
}
