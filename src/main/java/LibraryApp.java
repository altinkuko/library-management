import Repository.BookRepository;
import models.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import utils.HibernateUtils;

import java.util.Scanner;

public class LibraryApp {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        BookRepository bookRepository = new BookRepository(session);
        System.out.println(bookRepository.read());
        System.out.println("Vendos Id e librit qe do te ndryshosh");
        Scanner in = new Scanner(System.in);
        int id = in.nextInt();
        Book book = bookRepository.findById(id);
        System.out.println("Vendos nje titull te ri per librin");
        String bookTitle = in.nextLine();
        book.setTitle(bookTitle);
        bookRepository.update(id);
        System.out.println(book);
    }

}
